﻿using System.Collections.Generic;

namespace TQAHelpers.Infrastructure
{
    /// <summary>
    /// Stripped down version of Ariel RecordServer TaggedParameters.
    /// Allows storage of a collection of objects keyed by a name string.
    /// Attempts to add a duplicate name will result in an exception.
    /// 
    /// Internally, this class uses a Dictionary of string keys and System.Objects.
    /// </summary>
    public class TaggedParameters
    {
        #region Members
        private Dictionary<string, object> m_Dict = null;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public TaggedParameters()
        {
            m_Dict = new Dictionary<string, object>();
        }
        #endregion

        #region Add Functions
        /// <summary>
        /// Add a string Tag
        /// </summary>
        /// <param name="Tag">Tag name</param>
        /// <param name="Value">String value</param>
        /// <remarks>Example: Row.AddTag("Email", "tqa@etxcapital.com");</remarks>
        public void AddTag(string Tag, string Value)
        {
            m_Dict.Add(Tag, Value);
        }

        /// <summary>
        /// Add a bool tag
        /// </summary>
        /// <param name="Tag">Tag name</param>
        /// <param name="Value">true or false</param>
        /// <remarks>Example: Row.AddTag("Over50", true);</remarks>
        public void AddTag(string Tag, bool Value)
        {
            m_Dict.Add(Tag, Value);
        }

        /// <summary>
        /// Add an Object tag
        /// </summary>
        /// <param name="Tag">Tag name</param>
        /// <param name="Value">Any object</param>
        /// <remarks>Example: Row.AddTag("Names", ListOfNames);</remarks>
        public void AddTag(string Tag, object Value)
        {
            m_Dict.Add(Tag, Value);
        }
        #endregion

        #region Get Functions
        /// <summary>
        /// Get a string tag
        /// </summary>
        /// <param name="Tag">Tag name</param>
        /// <returns>String if tag is found or an empty string</returns>
        /// <remarks>Example: string EmailAddress = GetTag("Email");</remarks>
        public string GetTag(string Tag)
        {
            if (m_Dict.ContainsKey(Tag))
                return m_Dict[Tag] as string;

            return "";
        }

        /// <summary>
        /// Get a bool tag
        /// </summary>
        /// <param name="Tag">Tag name</param>
        /// <param name="Default">Default value if tag not found</param>
        /// <returns>The bool value of the tag or the default if not found</returns>
        /// <remarks>Example: if(GetBoolTag("Over50", false)){...}</remarks>
        public bool GetBoolTag(string Tag, bool Default)
        {
            if (m_Dict.ContainsKey(Tag))
                return (bool)m_Dict[Tag];

            return Default;
        }

        /// <summary>
        /// Get the raw object
        /// </summary>
        /// <param name="Tag">Tag name</param>
        /// <returns>The raw object or null if tag not found</returns>
        /// <remarks>Example: ListOfFirstNames = GetTagRaw(ListOfNames);</remarks>
        public object GetTagRaw(string Tag)
        {
            if (m_Dict.ContainsKey(Tag))
                return m_Dict[Tag];

            return null;
        }

        /// <summary>
        /// Gets the list is keys in the TaggedParameters
        /// </summary>
        /// <returns>List of key names</returns>
        public List<string> GetKeys()
        {
            return new List<string>(m_Dict.Keys);
        }
        #endregion
    }
}
