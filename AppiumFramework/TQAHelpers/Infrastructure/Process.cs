﻿using System.Diagnostics;

namespace TQAHelpers.Infrastructure
{
    /// <summary>
    /// Wraps up Process handling
    /// </summary>
    public class Procs
    {
        /// <summary>
        /// Kill all processes with the specified name
        /// </summary>
        /// <param name="Name">Name of process (e.g. 'Chrome', 'MicrosoftEdge')</param>
        public static void KillAllProcesses(string Name)
        {
            foreach (Process Proc in Process.GetProcessesByName(Name))
                Proc.Kill();
        }
    }
}
