﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Xml;

using TQAHelpers.Infrastructure;

namespace TQAHelpers.Data
{
    /// <summary>
    /// Class to handle all aspects of the data (Files, SQL etc)
    /// </summary>
    public class DataHandling
    {
        #region Static Converters
        /// <summary>
        /// Convert a string into a decimal
        /// </summary>
        /// <param name="Data">Incoming data string</param>
        /// <param name="RemoveSymbol">true if the string begins with a currency symbol</param>
        /// <param name="NegativeBrackets">true if a negative value is surrounded with brackets</param>
        /// <returns>The string expressed as a decimal</returns>
        static public decimal Convert(string Data, bool RemoveSymbol = true, bool NegativeBrackets = true)
        {
            decimal ReturnVal = 0m;
            int Multiplier = 1;

            // Remove any commas (assumes comma is thousand separator)
            string TempVal = Data.Replace(",", "");

            try
            {
                if (RemoveSymbol)
                    TempVal = TempVal.Substring(1);

                if (NegativeBrackets && TempVal.EndsWith(")"))
                {
                    TempVal = TempVal.Substring(1, TempVal.Length - 2);
                    Multiplier = -1;
                }

                ReturnVal = decimal.Parse(TempVal) * Multiplier;
            }
            catch
            {
                // Fall through and return zero
            }

            return ReturnVal;
        }

        /// <summary>
        /// Extracts the numeric part of a string, which must be positive and at the beginning of the string.
        /// e.g. "100BUY" or "4SELL".
        /// </summary>
        /// 
        /// <param name="RawValue">String with leading numeric component</param>
        /// <param name="RemainingString">The bit after the number</param>
        /// <returns>Leading decimal number</returns>
        public static decimal ExtractValue(string RawValue, out string RemainingString)
        {
            decimal ReturnVal = -1.0M;
            string ValidPart = "";

            RemainingString = "";

            for (int CharCount = 0; CharCount < RawValue.Length; CharCount++)
            {
                if (RawValue[CharCount] == '.')
                {
                    ValidPart += '.';
                    continue;
                }

                string s = RawValue[CharCount].ToString();
                int i;

                if (int.TryParse(s, out i))
                {
                    ValidPart += s;
                    continue;
                }

                break;
            }

            if (ValidPart.Length > 0)
                ReturnVal = decimal.Parse(ValidPart);

            RemainingString = RawValue.Substring(ValidPart.Length);

            return ReturnVal;
        }
        #endregion

        #region File Handling
        /// <summary>
        /// Reads a File and returns the lines as list of strings.
        /// </summary>
        /// <param name="OutputFilename">Name of file to read</param>
        /// <param name="bIncludeBlankLines">Returns empty strings if true</param>
        /// <returns>Lines as items in a list of strings</returns>
        static public List<string> ReadFile(string Filename, bool bIncludeBlankLines=true)
        {
            try
            {
                List<string> Lines = new List<string>();

                // Create an instance of StreamReader to read from a TempFile.
                // The using statement also closes the StreamReader.
                using (StreamReader sr = new StreamReader(Filename))
                {
                    string Line;
                    // Read lines from the TempFile until the end is reached.
                    while ((Line = sr.ReadLine()) != null)
                    {
                        if (!bIncludeBlankLines && Line == "")
                            continue;

                        // Add line to the list
                        Lines.Add(Line);
                    }
                }

                // Return the list
                return Lines;
            }
            catch
            {
                return null;
            }
		}

        /// <summary>
        /// Appends a line to a file. Creates the Folders and File if required.
        /// </summary>
        /// <param name="OutputFilename">Output file name</param>
        /// <param name="Line">Line to write to the file</param>
        static public void WriteFile(string OutputFilename, string Line)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(OutputFilename));
            }
            catch
            {
                //Fall through
            }

            using (FileStream Stream = new FileStream(OutputFilename, FileMode.Append))
            {
                using (StreamWriter SWriter = new StreamWriter(Stream))
                {
                    SWriter.WriteLine(Line);
                }
            }
        }

        /// <summary>
        /// Appends a list of lines to a file. Creates the Folders and File if required.
        /// </summary>
        /// <param name="OutputFilename">Output file name</param>
        /// <param name="Lines">List of lines to write to the file</param>
        /// <param name="Overwrite">'true' forces overwriting of an existing file</param>
        static public void WriteFile(string OutputFilename, List<string> Lines, bool Overwrite=false)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(OutputFilename));
            }
            catch
            {
                //Fall through
            }

            // Set File open FileMode
            FileMode Mode;
            if (Overwrite)
                Mode = FileMode.Create;
            else
                Mode = FileMode.Append;

            using (FileStream Stream = new FileStream(OutputFilename, Mode))
            {
                using (StreamWriter SWriter = new StreamWriter(Stream))
                {
                    foreach(var Line in Lines)
                        SWriter.WriteLine(Line);
                }
            }
        }

        /// <summary>
        /// Split a tab separated line into a List of strings.
        /// </summary>
        /// <param name="Line">Tab seaparated line</param>
        /// <returns>List of strings</returns>
        static public List<string> SplitTSVLine(string Line)
        {
            string[] ValuesArray = Line.Split('\t');

            List<string> ValuesList = new List<string>(ValuesArray);

            return ValuesList;
        }

        /// <summary>
        /// Split a comma separated line into a List of strings. '~' is converted into a comma.
        /// This allows values with commas in them to be handled correctly (eg $1,000 should be $1~000)
        /// </summary>
        /// <param name="Line">Comma seaparated line</param>
        /// <param name="ConvertTildeToComma">If true, '~' is converted into a comma.</param>
        /// <returns>List of strings</returns>
        static public List<string> SplitCSVLine(string Line, bool ConvertTildeToComma = true)
        {
            string[] ValuesArray = Line.Split(',');

            for (int Count = 0; Count < ValuesArray.Length; Count++)
                ValuesArray[Count] = ValuesArray[Count].Replace('~', ',');

            List<string> ValuesList = new List<string>(ValuesArray);

            return ValuesList;
        }

        /// <summary>
        /// Returns a list of Attributes from an XML file
        /// </summary>
        /// <param name="Filename">File to open</param>
        /// <param name="AttributeName">Name of attributes to retrieve</param>
        /// <param name="ElementName">Name of Element or default "" if all elements</param>
        /// <returns>List of Attributes matching the criteria</returns>
        static public Dictionary<string, string> GetXMLAttributes(string Filename, string AttributeName, string ElementName = "")
        {
            Dictionary<string, string> Attributes = new Dictionary<string, string>();
            XmlTextReader Reader = new XmlTextReader(Filename);

            while (Reader.Read())
            {
                switch (Reader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        if (ElementName == "" || Reader.Name == ElementName)
                        {
                            string Attribute = Reader.GetAttribute(AttributeName);
                            string Value = Reader.GetAttribute("value");

                            if (Attribute != null)
                            {
                                //if(Attributes.ContainsKey(Attribute))
                                //{
                                //    string s = "stop";
                                //}

                                Attributes[Attribute] = Value;
                            }
                        }
                        break;
                    case XmlNodeType.Text: //Display the text in each element.
                        break;
                    case XmlNodeType.EndElement: //Display the end of the element.
                        break;
                }
            }

            return Attributes;
        }

        /// <summary>
        /// Gets all filenames from a starting folder with specified extensions
        /// </summary>
        /// <param name="Folder">Starting folder</param>
        /// <param name="Filenames">Returned Filenames</param>
        /// <param name="ExtensionToInclude">A single extension to Include</param>
        static public void GetFilenames(string Folder, string ExtensionToInclude, ref List<string> Filenames)
        {
            List<string> ExtensionsToInclude = new List<string>();
            ExtensionsToInclude.Add(ExtensionToInclude);

            GetFilenames(Folder, ExtensionsToInclude, ref Filenames);
        }


        /// <summary>
        /// Gets all filenames from a starting folder with specified extensions
        /// </summary>
        /// <param name="Folder">Starting folder</param>
        /// <param name="Filenames">Returned Filenames</param>
        /// <param name="ExtensionsToInclude">A list of extensions to Include or all if not set</param>
        static public void GetFilenames(string Folder, List<string> ExtensionsToInclude, ref List<string> Filenames)
        {
            List<string> Directories = new List<string>(Directory.GetDirectories(Folder));

            foreach (string Directory in Directories)
                GetFilenames(Directory, ExtensionsToInclude, ref Filenames);

            string[] Files = Directory.GetFiles(Folder);
            foreach (string Filename in Files)
            {
                if (ExtensionsToInclude == null || ExtensionsToInclude.Count == 0)
                    Filenames.Add(Filename);
                else
                {
                    foreach (string Extension in ExtensionsToInclude)
                    {
                        if (Filename.EndsWith(Extension))
                        {
                            Filenames.Add(Filename);
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns the IP address of the local machine
        /// </summary>
        /// <returns>IP address in a.b.c.d format or throws an exception</returns>
        static public string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            throw new Exception("Local IP Address Not Found!");
        }
        #endregion

        #region SQL
        /// <summary>
        /// Runs a SQL command returning each row as a list of tagged parameters
        /// </summary>
        /// <param name="sDSN">Connection string</param>
        /// <param name="SQLCommand">Command</param>
        /// <returns>List of TaggedParameters (A set per returned row)</returns>
        static public List<TaggedParameters> RunAnSQLCommand(string sDSN, string SQLCommand)
        {
            List<TaggedParameters> Rows = null;
            DbConnection dbConn = null;

            try
            {
                DbProviderFactory dbproviderfactory = DbProviderFactories.GetFactory("System.Data.SqlClient");
                dbConn = dbproviderfactory.CreateConnection();
                dbConn.ConnectionString = sDSN;

                DbCommand cmd = dbproviderfactory.CreateCommand();
                cmd.Connection = dbConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = SQLCommand;

                // Open the DB connection
                dbConn.Open();
                DbDataReader dataReader = cmd.ExecuteReader();

                if (dataReader.HasRows)
                {
                    Rows = new List<TaggedParameters>();
                    List<string> Headers = new List<string>();
                    bool FirstRow = true;

                    while (dataReader.Read())
                    {
                        TaggedParameters Row = new TaggedParameters();
                        if (FirstRow)
                        {
                            FirstRow = false;
                            for (int Column = 0; Column < dataReader.FieldCount; Column++)
                            {
                                Headers.Add(dataReader.GetName(Column));
                            }
                        }

                        for (int Column = 0; Column < dataReader.FieldCount; Column++)
                        {
                            Row.AddTag(Headers[Column], dataReader.GetValue(Column).ToString());
                        }

                        Rows.Add(Row);
                    }
                }

                dbConn.Close();
            }
            catch(Exception Err)
            {
                dbConn.Close();
                throw (new Exception((string.Format("RunAnSQLCommand() DB Exception: {0}", Err.Message))));
            }

            return Rows;
        }
        #endregion

        #region Numbers
        /// <summary>
        /// Round UP. Always round anything more than zero up to the next value,
        /// as opposed to anything greater than or equal to 5.
        /// </summary>
        /// <param name="Number">Number to round UP</param>
        /// <param name="Places">Number of DP</param>
        /// <returns>Rounded decimal</returns>
        public static decimal RoundUp(decimal Number, int Places)
        {
            decimal factor = RoundFactor(Places);
            Number *= factor;
            Number = Math.Ceiling(Number);
            Number /= factor;
            return Number;
        }

        private static decimal RoundFactor(int Places)
        {
            decimal factor = 1m;

            if (Places < 0)
            {
                Places = -Places;
                for (int i = 0; i < Places; i++)
                    factor /= 10m;
            }

            else
            {
                for (int i = 0; i < Places; i++)
                    factor *= 10m;
            }

            return factor;
        }
        #endregion
    }
}
