﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using TQAHelpers.Data;

namespace TQAHelpers.Reporting
{
    /// <summary>
    /// Handles creation of an HTML file, starting and ending tests, Logging additional information and writing the file
    /// </summary>
    public class Report
    {
        #region Members
        #region HTML Templates
        private const string m_ResultHeadingsFormat = "<html><header><title>TQAAutomation</title></header><body><br><b>Test Suite: {0} for {1} ({2})</b><br>\n";
        private const string m_StartTestFormat = "<br>Results for <b>{0}  ({1}) </b><table style=\"table-layout: fixed\" border=\"1\"><tr style=\"background-color:#99CCFF\"><td style=\"width:5%\"><b>Step</b></td><td style=\"width:80%\"><b>Comment</b></td><td style=\"width:5%\"><b>Result</b></td></tr>\n";
        private const string m_EndTestFormat = "</table><br>\n";
        private const string m_ResultFooterFormat = "</body></html>\n";
        private const string m_SuccessFormat = "<tr><td>{0}</td><td>{1}</td><td style=\"background-color:#00FF00\">Pass</td></tr>\n";
        private const string m_WarningFormat = "<tr><td>{0}</td><td>{1}</td><td style=\"background-color:#F5C35D\">Warn</td></tr>\n";
        private const string m_FailureFormat = "<tr><td>{0}</td><td>{1}</td><td style=\"background-color:#FF0000\">Fail</td></tr>\n";
        private const string m_TotalsFormat = "Total Scenarios for {0}: {1}<br>Tests: Total Succeeded: {2}. Total Warnings: {3}. Total Failed: {4}.\n";

        /// <summary>
        /// Scenario Count,
        /// Succeeded Total,
        /// Warning Total,
        /// Failed Total
        /// </summary>
        private const string m_ChartTotalsFormat = "{0}<br>{1}<br>{2}<br>{3}";

        private const string m_CommentFormat = "<!--{0} {1}-->\n";
        #endregion

        private List<string> m_ReportLog = null;
        private string m_OutputFilename = "";
        private string m_TestResult = "";
        private string m_TestSuite = "";
        private string m_TestCase = "";
        private int m_ScenarioCount = 0;
        private int m_SuccessCount = 0;
        private int m_WarningCount = 0;
        private int m_FailureCount = 0;
        private int m_TestSuffix = 0;
        #endregion

        #region Public functions
        /// <summary>
        /// Report Constructor
        /// </summary>
        /// <param name="TestSuite">Name of suite of tests (normally the name of the script file)</param>
        /// <remarks>The 'TestSuite' name will be used as part of the output filename, along with the date. eg '20161225ChristmasReport.html'</remarks>
        public Report(string TestSuite, string Browser = "Unknown")
        {
            m_TestSuite = TestSuite;
            m_OutputFilename = string.Format("{0}-{1}.html", Browser, Path.GetFileNameWithoutExtension(m_TestSuite));

            string Prefix = DateTime.Now.ToString("yyyyMMddHHmmss");

            m_OutputFilename = string.Format("{0}\\Journals\\{1}{2}", TestContext.CurrentContext.TestDirectory, Prefix, m_OutputFilename);

            m_TestResult = string.Format(m_ResultHeadingsFormat, TestSuite, Browser, DateTime.Now.ToString());

            m_ReportLog = new List<string>();
            LogMessage("Start of Report");

            StartTest();
        }

        /// <summary>
        /// Sets the name of the testcase that Successes and Failures will be reported against
        /// </summary>
        /// <param name="TestCase">Name of test case</param>
        /// <remarks>Example: m_Report.TestCase("BigTest");</remarks>
        public void TestCase(string TestCase)
        {
            // Don't create a new testcase if the new one has the same name as the last
            if (m_TestCase == TestCase)
                return;

            m_ScenarioCount++;

            // First time don't try and close off the test
            if (m_TestCase != "")
            {
                m_TestResult += string.Format(m_EndTestFormat);
            }

            m_TestCase = TestCase;
            m_TestSuffix = 0;

            m_TestResult += string.Format(m_StartTestFormat, TestCase, DateTime.Now.ToString());
            LogMessage(string.Format("--------New Test Case - {0}", TestCase));

            WriteFile();
            m_TestResult = "";
        }

        /// <summary>
        /// Closes the output file after writing the footer
        /// </summary>
        public void EndReport()
        {
            EndTest();

            m_TestResult += m_ResultFooterFormat;
            LogMessage("End of Report");
            WriteFile();
        }

        /// <summary>
        /// Logs the success the current test case
        /// </summary>
        /// <param name="Message">Message to add to the log</param>
        /// <remarks>Example: m_Report.Success("All tests passed");</remarks>
        public void Success(string Message)
        {
            m_TestResult += string.Format(m_SuccessFormat, ++m_TestSuffix, Message);
            m_SuccessCount++;

            LogMessage(string.Format("{0}. {1}", m_TestSuffix, Message));
        }

        /// <summary>
        /// Logs a warning for the current test case
        /// </summary>
        /// <param name="Message">Message to add to the log</param>
        /// <remarks>Example: m_Report.Warning("RO-1234 Known bug");</remarks>
        public void Warning(string Message)
        {
            m_TestResult += string.Format(m_WarningFormat, ++m_TestSuffix, Message);
            m_WarningCount++;

            LogMessage(string.Format("{0}. {1}", m_TestSuffix, Message));
        }

        /// <summary>
        /// Logs the Failure of a step in the current test case
        /// </summary>
        /// <param name="Action">Action being performed within the test case</param>
        /// <param name="Message">Message to add to the log</param>
        /// <remarks>Example: m_Report.Failure("Account Pending", "Account ID should not be available");</remarks>
        public void Failure(string Action, string Message)
        {
            string TestCase = string.Format("{0}: {1}", ++m_TestSuffix, Action);
            m_TestResult += string.Format(m_FailureFormat, TestCase, Message);
            m_FailureCount++;

            LogMessage(string.Format("Failure of {0} - {1}", TestCase, Message));
        }

        /// <summary>
        /// Logs a timed message as a comment in the output file and saves it in a list
        /// </summary>
        /// <param name="Message">Message to log</param>
        /// <remarks>Example: m_Report.LogMessage("Front of card filled in successfully");</remarks>
        public void LogMessage(string Message)
        {
            string Now = DateTime.Now.ToString();
            m_TestResult += string.Format(m_CommentFormat, Now, Message);
            m_ReportLog.Add(string.Format("{0} {1}", Now, Message));

            Console.WriteLine(Message);
        }

        /// <summary>
        /// Gets the List that constitutes the log
        /// </summary>
        /// <returns>List of strings representing the log</returns>
        public List<string> GetLog()
        {
            return m_ReportLog;
        }
        #endregion

        #region Private functions
        /// <summary>
        /// Opens the Output file and writes the contents of the test results
        /// </summary>
        private void WriteFile()
        {
            DataHandling.WriteFile(m_OutputFilename, m_TestResult);
        }

        /// <summary>
        /// Sets up a new test table with headings
        /// </summary>
        private void StartTest()
        {
            m_SuccessCount = 0;
            m_WarningCount = 0;
            m_FailureCount = 0;

            LogMessage(string.Format("New Test Suite - {0}", m_TestSuite));
        }

        /// <summary>
        /// Closes the table for the current test suite
        /// </summary>
        private void EndTest()
        {
            m_TestResult += string.Format(m_EndTestFormat);
            m_TestResult += string.Format(m_TotalsFormat, m_TestSuite, m_ScenarioCount, m_SuccessCount, m_WarningCount, m_FailureCount);

            m_TestResult += "<br><br>Totals for chart<br>";
            m_TestResult += string.Format(m_ChartTotalsFormat, m_ScenarioCount, m_SuccessCount, m_WarningCount, m_FailureCount);

            LogMessage("End of test");
        }
        #endregion
    }
}
