﻿using AppiumAuto.Pages;
using OpenQA.Selenium.Appium.Android;
using TechTalk.SpecFlow;

namespace AppiumAuto.Steps
{
    [Binding]
    public class AuthorisedClientSteps
    {
        RegistrationPage RegPage = null;

        [Given(@"User on App Home Page")]
        public void GivenUserOnAppHomePage()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"Click Open Live Account")]
        public void GivenClickOpenLiveAccount()
        {
            HomePage homePage = new HomePage();
            RegPage = homePage.ClickLiveAccount();
        }

        [When(@"User Try to Sign Up")]
        public void WhenUserTryToSignUp()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"User will be authorised Client")]
        public void ThenUserWillBeAuthorisedClient()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
