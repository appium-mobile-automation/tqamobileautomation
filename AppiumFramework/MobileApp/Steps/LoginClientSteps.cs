﻿using AppiumAuto.Pages;
using MobileApp.Hooks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace AppiumAuto.Steps
{
    [Binding]
    public class LoginClientSteps
    {
        LoginPage loginPage = null;
        HomePage homePage = new HomePage();
        TradingPage tradingPage = new TradingPage();
        
        [Given(@"User on Home Page")]
        public void GivenUserOnHomePage()
        {
            var homeScreen = homePage.IsHomeScreenExist();
            Assert.That(homeScreen, Is.True, "Home Screen Does Exist");
        }

        [Given(@"Click on Login To Your Account")]
        public void GivenClickOnLoginToYourAccount()
        {
            loginPage = homePage.ClickLogin();
        }

        [Then(@"User Can See The Sign In Button")]
        public void ThenUserCanSeeTheSignInButton()
        {
            var btnLoginExist = loginPage.IsLoginInButtonExist();
            Assert.That(btnLoginExist, Is.False, "Login Button Does Not Exist");
        }


        [When(@"User Enter UserName and Password")]
        public void WhenUserEnterUserNameAndPassword(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            loginPage.Login((string)data.UserName, (string)data.Password);
        }

        [Then(@"User will be Logged In")]
        public void ThenUserWillBeLoggedIn()
        {
            var TradingExist = tradingPage.IsTradingPlatformExist;
            Assert.That(TradingExist, Is.True, "Trading Platform Displayed");
        }
    }
}
