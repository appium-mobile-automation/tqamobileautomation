﻿using AppiumBase.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Support.PageObjects;

namespace AppiumAuto.Pages
{
    class HomePage : BasePage
    {
        AppiumWebElement LoginToYourAccount => AppiumDriver.FindElementById("login_Btn");
        internal LoginPage ClickLogin()
        {
            LoginToYourAccount.Click();
            return new LoginPage();
        }
        AppiumWebElement OpenALiveAccount => AppiumDriver.FindElementById("liveAccount_Btn");
        internal RegistrationPage ClickLiveAccount()
        {
            OpenALiveAccount.Click();
            return new RegistrationPage();
        }
        AppiumWebElement OpenADemoAccount => AppiumDriver.FindElementById("demoAccount_Btn");

        internal DemoRegistrationPage ClickDemoAccount()
        {
            OpenADemoAccount.Click();
            return new DemoRegistrationPage();
        }

        AppiumWebElement HomeScreen => AppiumDriver.FindElementById("com.app.android.ariel.etx:id/logoImg");
        internal bool IsHomeScreenExist()
        {
            return HomeScreen.Displayed;
        }
    }
}
