﻿using AppiumBase.Base;
using OpenQA.Selenium.Appium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace AppiumAuto.Pages
{
    class TradingPage : BasePage
    {
        private AppiumWebElement GetTradingPlatform()
        {
            Thread.Sleep(5000);
            return AppiumDriver.FindElementById("bottomNavigationView");
        }

        internal bool IsTradingPlatformExist => GetTradingPlatform().Displayed;
    }
    
}
