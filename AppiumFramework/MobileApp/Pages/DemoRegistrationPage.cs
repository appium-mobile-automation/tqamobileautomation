﻿using AppiumBase.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace AppiumAuto.Pages
{
    class DemoRegistrationPage : DriverFactory
    {
        AppiumWebElement LoginButton => AppiumDriver.FindElementById("login");
        AppiumWebElement txtEmail => AppiumDriver.FindElementById("emailET");
        AppiumWebElement txtPassword => AppiumDriver.FindElementById("passwordET");

        internal void Login(string email, string password)
        {
            Thread.Sleep(2000);
            txtPassword.SendKeys(password);
            txtEmail.SendKeys(email);
            AppiumDriver.HideKeyboard();
            LoginButton.Click();
        }
    }
}
