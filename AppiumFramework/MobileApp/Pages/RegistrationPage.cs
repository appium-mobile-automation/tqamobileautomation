﻿using AppiumBase.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace AppiumAuto.Pages
{
    class RegistrationPage : DriverFactory
    {
        AppiumWebElement FirstName => AppiumDriver.FindElementById("FirstName");
        AppiumWebElement LastName => AppiumDriver.FindElementById("LastName");
        AppiumWebElement EmailAddress => AppiumDriver.FindElementById("EmailAddress");
        AppiumWebElement Password => AppiumDriver.FindElementById("Password");

        internal void Register(string email, string password)
        {
            Thread.Sleep(2000);
            AppiumDriver.HideKeyboard();
        }
    }
}
