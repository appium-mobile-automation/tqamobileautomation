﻿using AppiumBase.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace AppiumAuto.Pages
{
    class LoginPage : BasePage
    {
        private AppiumWebElement GetLoginButton()
        {
            return AppiumDriver.FindElementById("login");
        }

        AppiumWebElement txtEmail => AppiumDriver.FindElementById("emailET");
        AppiumWebElement txtPassword => AppiumDriver.FindElementById("passwordET");


        internal void Login(string email, string password)
        {

            txtPassword.SendKeys(password); 
            txtEmail.SendKeys(email);
            
            Thread.Sleep(2000);
            AppiumDriver.HideKeyboard();
            Thread.Sleep(2000);
            GetLoginButton().Click();
        }

        internal bool IsLoginInButtonExist()
        {
            return GetLoginButton().Displayed;
        }
    }
}
