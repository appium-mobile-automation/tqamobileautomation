﻿using AppiumBase.Base;
using AppiumBase.Config;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using TechTalk.SpecFlow;

namespace MobileApp.Hooks
{
    [Binding]
    public class TestInitialize 
    {
        [BeforeScenario]
        public void InitializeTest()
        {
            ConfigReader.InitializeSettings();

            DriverFactory.Instance.InitializeAppiumDriver<AppiumDriver<AppiumWebElement>>(MobileType.Hybrid);
        }

        [AfterScenario]


        [TearDown]
        public void CleanUp()
        {
            DriverFactory.Instance.CloseAppiumContext();
            DriverFactory.Instance.AppiumDriver.CloseApp();
            DriverFactory.Instance.AppiumDriver.Dispose();
        }
    }
}
