﻿using System;
using OpenQA.Selenium.Appium;

namespace AppiumBase.Base
{
    public class BasePage : Base
    {
        public AppiumDriver<AppiumWebElement> AppiumDriver;

        public BasePage() => AppiumDriver = DriverFactory.Instance.AppiumDriver;

        public TPage GetInstance<TPage>() where TPage : BasePage, new()
        {   
            var T = Activator.CreateInstance(typeof(TPage));
            return (TPage)T;
        }

        public TPage As<TPage>() where TPage : BasePage => (TPage)this;
    }
}
